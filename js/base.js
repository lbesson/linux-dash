$(document).ready(function() {
	
    // Automatically add Google Analytics web service  
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    var my_id = 'UA-38514290-1',    my_domain = 'crans.org';
    /* Here a case-by-case analysis is done to change this variable
    * when the page is located on another web server. */
    switch(window.location.host) {
    case 'www.dptinfo.ens-cachan.fr':
        my_id = 'UA-38514290-2';  my_domain = 'dptinfo.ens-cachan.fr';
        break;
    case 'lbesson.bitbucket.org':
        my_id = 'UA-38514290-14'; my_domain = 'bitbucket.org';
        break;
    default:
        break; };
    /* New method. */
    ga('create', my_id, my_domain); ga('send', 'pageview');
    /* Old method. */
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', my_id]);
    // _gaq.push(['_setDomainName', my_domain]);
    _gaq.push(['_trackPageview']);
    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; 
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

	// enable popovers
	$(".pop").popover(); 
	
    // Smooth scrolling for links.
    $(".mainnav").on("click", ".js-smoothscroll", function(event) {
        event.preventDefault();
        var target = $(this.hash).parent();
        pulseElement(target, 8, 400);

        $("html").animate({
            scrollTop: target.offset().top - 130
        }, 1000);
    });
	
	// refresh all widgets
	$('#refresh-all').on('click',function(){
		dashboard.getAll();
	});
	
    dashboard.getAll();
}).on("click", ".js-refresh-info", function(event) {
    event.preventDefault();
    var item = event.target.id.split("-").splice(-1)[0];
    dashboard.fnMap[item]();
});

// Handle for cancelling active effect.
var pulsing = {
    element: null,
    timeoutIDs: [],
    resetfn: function() {
        pulsing.element = null;
        pulsing.timeoutIDs = [];
    }
};

/**
 * Applies a pulse effect to the specified element. If triggered while already
 * active the ongoing effect is cancelled immediately.
 *
 * @param {HTMLElement} element The element to apply the effect to.
 * @param {Number} times How many pulses.
 * @param {Number} interval Milliseconds between pulses.
 */
function pulseElement(element, times, interval) {
    if (pulsing.element) {
        pulsing.element.removeClass("pulse").
            parent().removeClass("pulse-border");
        pulsing.timeoutIDs.forEach(function(ID) {
            clearTimeout(ID);
        });
        pulsing.timeoutIDs = [];
    }
    pulsing.element = element;
    var parent = element.parent();
    var f = function() {
        element.toggleClass("pulse");
        parent.toggleClass("pulse-border");
    };

    pulsing.timeoutIDs.push(setTimeout(pulsing.resetfn,
                                       (times + 1) * interval));
    for (; times > 0; --times) {
        pulsing.timeoutIDs.push(setTimeout(f, times * interval));
    }
}
